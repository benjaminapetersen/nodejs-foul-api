# FROM node:alpine
FROM node:9.10-alpine

RUN echo "building...."

WORKDIR /usr/src/app

# install our dependencies as a layer
COPY package*.json yarn.lock ./
# don't install dev dependencies
# this can also be controlled by the NODE_ENV flag 
# see: https://yarnpkg.com/lang/en/docs/cli/install/#toc-yarn-install-production-true-false
RUN yarn install --production=true

# install our app... ADD vs COPY
# COPY ./ ./
ADD ./ ./

# TODO: wire this up somehow externally?
EXPOSE 8080 

CMD ["yarn", "start"]



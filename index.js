const express = require('express');
const app = express();

const PORT = 3000;

const foul = [
  { name: 'scrooge', says: 'quack', looksLikeADuck: true, swimsLikeADuck: true, quacksLikeADuck: true,  isVerifiedDuck: true, mightBeAChicken: false},
  { name: 'daffy', says: 'quack', looksLikeADuck: true, swimsLikeADuck: true, quacksLikeADuck: true,  isVerifiedDuck: true, mightBeAChicken: false},
  { name: 'cluck', says: 'cockadoodledoo', looksLikeADuck: false, swimsLikeADuck: false, quacksLikeADuck: true,  isVerifiedDuck: false, mightBeAChicken: true},
  { name: 'woodstock', says: '', looksLikeADuck: false, swimsLikeADuck: false, quacksLikeADuck: false,  isVerifiedDuck: false, mightBeAChicken: false},
  { name: 'henery hawk', says: 'squawk', looksLikeADuck: false, swimsLikeADuck: false, quacksLikeADuck: false,  isVerifiedDuck: false, mightBeAChicken: true},
];

app.get('/', (req, res) => {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write(`
    <html>
      <body>
        <h1>Foul API</h1>
        <p>Give it a try.  But read the source to find the endpoints. Cluck.</p>
      </body>
    </html>
  `);
  res.end();
})



// returns all foul
app.get('/foul', (req, res) => {
  res.json(foul)
});

// only returns confirmed ducks
app.get('/ducks', (req, res) => {
  res.json(foul.filter((bird) => bird.isVerifiedDuck ));
});

// returns 1 quack per duck.  count the quacks if you need a precise number :)
app.get('/ducks/count', (req, res) => {
  res.json(foul.filter((bird) => bird.isVerifiedDuck ).map((() => 'quack')));
});

app.post('/ducks', (req, res) => {
  // but do we trust its really a duck?  nah....
  res.write('You think thats a duck?!?!?');
  res.end();
});

app.listen(PORT, () => console.log('Foul API listening on', PORT));